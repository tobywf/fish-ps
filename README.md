Note: I have integrated these files into my [dotfiles](https://github.com/tobywf/dotfiles), so this repo won't be updated in future (probs).

fish-ps
=======

My personal fish shell config and a prompt based loosely on the most excellent [shellder](https://github.com/simnalamburt/shellder) prompt. Hint: You should probably use shellder.

It's probably bad practice keeping the config and prompt in one repo, however this does make provisioning my development machines much easier. Another quirk is that `fisherman` seems to shorten the repo name to just `ps`. I'm not sure if I like this or not.
